import pytest
from urllib2 import urlopen
# test for str1

def func(str):
    html_content = urlopen('https://the-internet.herokuapp.com/context_menu').read().decode('utf-8')
    return html_content.find(str)

def test_answer_str1():
    str1 = "Right-click in the box below to see one called 'the-internet'"
    assert func(str1) != -1


