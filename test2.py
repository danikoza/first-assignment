import pytest
from urllib2 import urlopen
# test for str2
def func(str):
    html_content = urlopen('https://the-internet.herokuapp.com/context_menu').read().decode('utf-8')
    return html_content.find(str)
    
def test_answer_str2():
    str2 = "Alibaba"
    assert func(str2) != -1
